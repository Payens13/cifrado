import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.security.*;

public class RSA {
    public static void main(String[] args) {
        String algoritmo = "RSA";
        try {
            KeyPairGenerator keygen = KeyPairGenerator.getInstance(algoritmo);
            KeyPair keyPair = keygen.genKeyPair();

            PublicKey publicKey = keyPair.getPublic();
            PrivateKey privateKey = keyPair.getPrivate();

            Cipher cipher = Cipher.getInstance(algoritmo);
            String mensaje = "Hola";

            //cifrado
            cipher.init(Cipher.ENCRYPT_MODE, privateKey);
            byte[] mensajeCifrado = cipher.doFinal(mensaje.getBytes());

            System.out.println(new String(mensajeCifrado));

            //descifrado
            cipher.init(Cipher.DECRYPT_MODE, publicKey);
            byte[] mensajeDescifrado = cipher.doFinal(mensajeCifrado);

            System.out.println(new String(mensajeDescifrado));



        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        } catch (NoSuchPaddingException e) {
            throw new RuntimeException(e);
        } catch (InvalidKeyException e) {
            throw new RuntimeException(e);
        } catch (IllegalBlockSizeException e) {
            throw new RuntimeException(e);
        } catch (BadPaddingException e) {
            throw new RuntimeException(e);
        }


    }
}
