import javax.crypto.*;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

public class Main {
    public static void main(String[] args) {

        String algoritmo = "AES";
        try {
            KeyGenerator keygen = KeyGenerator.getInstance(algoritmo);
            keygen.init(256);
            SecretKey key = keygen.generateKey();

            Cipher cipher = Cipher.getInstance(algoritmo);
            cipher.init(Cipher.ENCRYPT_MODE, key);

            String mensaje = "Hola que tal";
            byte[] comarca = cipher.doFinal(mensaje.getBytes());

            System.out.println(new String(comarca));

            cipher.init(Cipher.DECRYPT_MODE, key);
            byte[] descifrado = cipher.doFinal(comarca);
            System.out.println(new String(descifrado));


        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        } catch (NoSuchPaddingException e) {
            throw new RuntimeException(e);
        } catch (InvalidKeyException e) {
            throw new RuntimeException(e);
        } catch (IllegalBlockSizeException e) {
            throw new RuntimeException(e);
        } catch (BadPaddingException e) {
            throw new RuntimeException(e);
        }

    }
}